use std::f64::consts::PI;

use math::*;

use crate::{BLACK, Color};
use crate::plot::Plot;

pub struct Circle {
  pub color: Color,
  pub radius: i64,
  pub filled: bool,
}
pub struct Line {
  pub color: Color,
  pub radius: i64,
}

impl Circle {
  pub fn draw(&self, plot: &mut Plot, pt: Point) {
    let c = plot.frame.pt_to_px(pt);
    let r_test_outer = self.radius * self.radius - 1;
    let r_test_inner = if self.filled { -1 } else { (self.radius - 1) * (self.radius - 1) - 1 };

    for x in 0..((self.radius + 1) as i64) {
      for y in 0..((self.radius + 1) as i64) {
        let test = x * x + y * y - x - y;
        if test >= r_test_inner && test < r_test_outer {
          plot.put_pixel_safe(c.0 - x, c.1 - y, self.color);
          plot.put_pixel_safe(c.0 - x, c.1 + y, self.color);
          plot.put_pixel_safe(c.0 + x, c.1 - y, self.color);
          plot.put_pixel_safe(c.0 + x, c.1 + y, self.color);
        }
      }
    }
  }
}

impl Line {

  pub fn draw(&self, plot: &mut Plot, pt: Point, angle: f64) {
    let c = plot.frame.pt_to_px(pt);
    let r = self.radius as f64;
    let a = angle;
    let x1 = (c.0 as f64) - (r + 0.5) * a.cos();
    let x2 = (c.0 as f64) + (r + 0.5) * a.cos();
    let y1 = (c.1 as f64) - (r + 0.5) * a.sin();
    let y2 = (c.1 as f64) + (r + 0.5) * a.sin();
    let dx = x2 - x1;
    let dy = y2 - y1;

    if a < (0.25 * PI) || a >= (1.75 * PI) {
      let px_x_1 = x1.round() as i64;
      let px_x_2 = x2.round() as i64;
      for x in px_x_1..px_x_2 {
        let y = y1 + dy * (x as f64 - x1) / dx;
        plot.put_pixel_safe(x, y.round() as i64, self.color);
      }
    } else if a < (0.75 * PI) {
      let px_y_1 = y1.round() as i64;
      let px_y_2 = y2.round() as i64;
      for y in px_y_1..px_y_2 {
        let x = x1 + dx * (y as f64 - y1) / dy;
        plot.put_pixel_safe(x.round() as i64, y, self.color);
      }
    } else if a < (1.25 * PI) {
      let px_x_1 = x1.round() as i64;
      let px_x_2 = x2.round() as i64;
      for x in px_x_2..px_x_1 {
        let y = y1 + dy * (x as f64 - x1) / dx;
        plot.put_pixel_safe(x, y.round() as i64, self.color);
      }
    } else {
      let px_y_1 = y1.round() as i64;
      let px_y_2 = y2.round() as i64;
      for y in px_y_2..px_y_1 {
        let x = x1 + dx * (y as f64 - y1) / dy;
        plot.put_pixel_safe(x.round() as i64, y, self.color);
      }
    }
    plot.put_pixel_safe(x2.round() as i64, y2.round() as i64, BLACK);
  }

}
