use std::fmt::Debug;
use std::process::Command;
use image::Rgb;

pub type Color = Rgb<u8>;
pub const RED: Color = Rgb([243, 97, 97]);
pub const ORANGE: Color = Rgb([247, 184, 85]);
pub const YELLOW: Color = Rgb([243, 243, 97]);
pub const GREEN: Color = Rgb([150, 217, 77]);
pub const BLUE: Color = Rgb([129, 196, 238]);
pub const PURPLE: Color = Rgb([151, 93, 201]);
pub const PINK: Color = Rgb([221, 95, 114]);
pub const WHITE: Color = Rgb([255, 255, 255]);
pub const BLACK: Color = Rgb([0, 0, 0]);
pub const MAUVE: Color = Rgb([199, 171, 175]);
pub const GREY: Color = Rgb([226, 215, 216]);

#[derive(Debug, Clone)]
struct TwoD<T: Debug + Clone> {
  pub x: T,
  pub y: T,
}

impl<T: Debug + Clone> TwoD<T> {
  pub fn new(x: T, y: T) -> TwoD<T> {
    TwoD { x, y }
  }
}

impl<R, T> From<(R, R)> for TwoD<T>
where T: Debug + Clone,
      R: Into<T> {
  fn from((x, y): (R, R)) -> TwoD<T> {
    TwoD::new(x.into(), y.into())
  }
}

mod plot_frame;
mod plot;
mod shapes;

pub use shapes::*;
pub use plot::*;

pub fn generate_movie(dirname: &str, extension: &str, delay: u32) {
  Command::new("convert")
    .arg("-delay")
    .arg(&format!("{}", delay))
    .arg(&format!("{}/*.{}", dirname, extension))
    .arg(&format!("{}/movie.gif", dirname))
    .output()
    .expect("Failed to execute image magik process");
}
