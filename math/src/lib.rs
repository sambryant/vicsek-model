use std::f64::consts::PI;
use std::fmt::Debug;
use std::ops::{Add, Sub, Mul};

pub const TWO_PI: f64 = 2.0 * PI;

pub fn mean_angle(angles: &[f64]) -> f64 {
  let norm = 1.0 / (angles.len() as f64);
  let x = angles.iter().map(|a| a.cos()).sum::<f64>() * norm;
  let y = angles.iter().map(|a| a.sin()).sum::<f64>() * norm;
  y.atan2(x)
}

pub fn compute_avg_angle_weighted(weighted_angles: &[(f64, f64)]) -> f64 {
  let mut x = 0.0;
  let mut y = 0.0;
  for (angle, weight) in weighted_angles {
    x += angle.cos() * weight;
    y += angle.sin() * weight;
  }
  y.atan2(x)
}

#[derive(Debug, Clone, Copy)]
pub struct Vector {
  pub x: f64,
  pub y: f64,
}

#[derive(Debug, Clone, Copy)]
pub struct Point {
  pub x: f64,
  pub y: f64,
}

impl Point {
  pub fn new(x: f64, y: f64) -> Self {
    Self { x, y }
  }

  pub fn bound_periodic(&mut self, x_max: f64, y_max: f64) {
    if self.x < 0.0 || self.x >= x_max {
      self.x = self.x.rem_euclid(x_max);
    }
    if self.y < 0.0 || self.y >= y_max {
      self.y = self.y.rem_euclid(y_max);
    }
  }
}

impl Vector {

  pub fn new(x: f64, y: f64) -> Self {
    Self { x, y }
  }

  pub fn norm(&self) -> f64 {
    (self.x * self.x + self.y * self.y).sqrt()
  }

}

impl Sub for &Point {
  type Output = Vector;

  fn sub(self, other: Self) -> Self::Output {
    Vector { x: self.x - other.x, y: self.y - other.y }
  }

}

impl Add for &Vector {
  type Output = Vector;

  fn add(self, other: Self) -> Self::Output {
    Vector { x: self.x + other.x, y: self.y + other.y }
  }
}

impl Add<&Vector> for &Point {
  type Output = Point;

  fn add(self, other: &Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Add<&Vector> for Point {
  type Output = Point;

  fn add(self, other: &Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Add<Vector> for &Point {
  type Output = Point;

  fn add(self, other: Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Add<Vector> for Point {
  type Output = Point;

  fn add(self, other: Vector) -> Self::Output {
    Point::new(self.x + other.x, self.y + other.y)
  }
}

impl Sub for &Vector {
  type Output = Vector;

  fn sub(self, other: Self) -> Self::Output {
    Vector { x: self.x - other.x, y: self.y - other.y }
  }
}

impl Mul<f64> for Vector {
  type Output = Vector;

  fn mul(mut self, scalar: f64) -> Self::Output {
    self.x *= scalar;
    self.y *= scalar;
    self
  }
}

impl Mul for &Vector {
  type Output = f64;

  fn mul(self, other: Self) -> Self::Output {
    self.x * other.x + self.y * other.y
  }

}

impl From<(f64, f64)> for Vector {
  fn from((x, y): (f64, f64)) -> Self {
    Self { x, y }
  }
}

impl From<(f64, f64)> for Point {
  fn from((x, y): (f64, f64)) -> Self {
    Self { x, y }
  }
}
