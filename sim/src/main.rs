use plotter;

pub const OUTPUT_FORMAT: &'static str = "bmp"; // faster saving but larger files
pub const OUTPUT_DIR: &'static str = "output";

mod sim;
mod util;

use sim::AlignmentMethod::*;

fn ordered_example() {
  let dt = 0.01; // 10 ms
  let t_end = 10.0;
  let velocity = 0.4;
  let number = 1000;
  let disk = 0.05;
  let rotational_d = 1.0;
  let opts = sim::SimulationOptions::new(number, dt, t_end, velocity, rotational_d, HardDisk(disk));

  println!("Computing data");
  let output = sim::Simulation::new(opts, (400, 400)).run(OUTPUT_DIR, true);

  println!("Generating movie...");
  plotter::generate_movie(&output, OUTPUT_FORMAT, (dt * 1000 as f64).round() as u32);

  println!("Saved output files to {}", output);
}

fn example_sparse_short_range() {
  let dt = 0.01; // 10 ms
  let t_end = 10.0;
  let velocity = 1.0;
  let number = 300;
  let disk = 0.03;
  let rotational_d = 4.0;
  let opts = sim::SimulationOptions::new(number, dt, t_end, velocity, rotational_d, LinearDisk(disk));

  println!("Computing data");
  let output = sim::Simulation::new(opts, (400, 400)).run(OUTPUT_DIR, true);

  println!("Generating movie...");
  plotter::generate_movie(&output, OUTPUT_FORMAT, (dt * 1000 as f64).round() as u32);

  println!("Saved output files to {}", output);
}

fn test_example() {
  let dt = 0.01; // 10 ms
  let t_end = 40.0;
  let velocity = 1.0;
  let number = 500;
  let disk = 0.05;
  let rotational_d = 2.0;
  let opts = sim::SimulationOptions::new(number, dt, t_end, velocity, rotational_d, PeriodicDisk(disk, 2.0 * 0.628318));

  println!("Computing data");
  let output = sim::Simulation::new(opts, (200, 200)).run(OUTPUT_DIR, true);

  println!("Generating movie...");
  plotter::generate_movie(&output, OUTPUT_FORMAT, (dt * 1000 as f64).round() as u32);

  println!("Saved output files to {}", output);
}

fn main() {
  example_sparse_short_range();
  ordered_example();
}
