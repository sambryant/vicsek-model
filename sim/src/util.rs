use indicatif::{ProgressBar, ProgressStyle};

pub fn make_dir(dirname: &str) {
  if !std::path::Path::new(&dirname).exists() {
    std::fs::create_dir(&dirname)
      .expect(&format!("Couldnt create directory: {}", dirname));
  }
}

pub fn get_progress_bar_style() -> ProgressStyle {
  ProgressStyle::default_bar()
    .template("{msg} {bar:30.cyan/blue} [{eta} remaining]")
    .progress_chars("##-")
}

pub fn get_progress_bar(msg: &'static str, size: u64, verbose: bool) -> ProgressBar {
  if verbose {
    let bar = ProgressBar::new(size).with_style(get_progress_bar_style());
    bar.set_message(msg);
    bar
  } else { ProgressBar::hidden() }
}

/// Data structure which uses a buffer to perform buffered updates to data.
///
/// Explicitly, this structure keeps two copies of some generic data type, which
/// can be accesssed by the methods `old()` and `new()`. The idea is that during
/// the course of an algorithm, you read from the `old` values and write to the
/// `new` values. Then after the step is finished, you call `swap_buffer` such
/// that `old` now points towards `new`.
///
/// Since you read from `old` and write to `new`, the methods `old()` and
/// `new()` return `&T` and `&mut T` respectively. This prevents any borrowing
/// conflicts.
///
/// This data structure provides three main benefits:
///   1. It makes the update step in an algorithm clean and explicit.
///   2. It allocates all memory used at initialization so the algorithm doesn't
///      have to allocate memory in each step.
///   3. It plays well with the rust borrow checker.
///
pub struct UpdateBuffer<T: Sized + Clone> {
  buffers: [T; 2],
  old_index: usize,
  new_index: usize,
}

impl<T: Sized + Clone> UpdateBuffer<T> {

  pub fn from(initial: T) -> UpdateBuffer<T> {
    UpdateBuffer {
      buffers: [initial.clone(), initial],
      old_index: 0,
      new_index: 1,
    }
  }

  pub fn new(&mut self) -> &mut T {
    &mut self.buffers[self.new_index]
  }

  pub fn old(&self) -> &T {
    &self.buffers[self.old_index]
  }

  pub fn swap_buffers(&mut self) {
    if self.old_index == 1 {
      self.old_index = 0;
      self.new_index = 1;
    } else {
      self.old_index = 1;
      self.new_index = 0;
    }
  }
}
